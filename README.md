# git

## How to?

Welcome to our mini GIT tutorial!

If you can read this Readme file, that means you have managed to:
- clone remote git repo
- open the project in IDE of your choosing
- find README file

Congratulations!


In this repository, you will find a file 'welcome.html' that contains a simple script asking a user for name and displaying a welcome message.
Make a change to this script, add some styling etc. (keep it simple) and commit your change WITHIN your own branch (set it to your own forename).
Push your commit to remote repository and check if it's online on Gitlab. 
Remember: NEVER push directly to MAIN branch.

What you will learn during the process:
- how to create an SSH key
- how to add SSH key to remote repository
- how to create your own branch
- how to add and commit changes (tip: use "add . " and "commit -m 'commit_message' ")
- how to push changes to remote repository
- how to create pull request within remote project
- why you should NEVER push to MAIN branch directly

Good luck!
If you are stuck, ask internet, ChatGPT or us for assistance :)

Your dev team